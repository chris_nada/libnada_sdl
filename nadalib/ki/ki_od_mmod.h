//
// Created by nada on 11.11.2017.
//
#pragma once

#include <iostream>
#include <dlib/dnn.h>
#include <dlib/data_io.h>
#include <dlib/gui_widgets.h>
#include "ki.h"

/**
 * http://dlib.net/dnn_mmod_ex.cpp.html
 */
class KI_OD_MMOD : private KI {

public:

    KI_OD_MMOD(const std::string& training_xml,
                const std::string& testing_xml)
            : training_xml(training_xml),
              testing_xml(testing_xml) {

    }

    void train() override {
        std::cout << "train() mit " << training_xml << ", " << testing_xml << std::endl;
        //Trainingsdaten laden
        std::vector<dlib::matrix<dlib::rgb_pixel>> images_train, images_test;
        std::vector<std::vector<dlib::mmod_rect>> face_boxes_train, face_boxes_test;
        dlib::load_image_dataset(images_train, face_boxes_train, training_xml);
        dlib::load_image_dataset(images_test, face_boxes_test, testing_xml);
        std::cout << "n training images = " << images_train.size() << std::endl;
        std::cout << "n testing  images = " << images_test.size()  << std::endl;

        //DNN Trainer konfig
        dlib::mmod_options options(face_boxes_train, 40, 40);
        net_type net(options);
        net.subnet().layer_details().set_num_filters(options.detector_windows.size());
        dlib::dnn_trainer<net_type> trainer(net);
        trainer.set_learning_rate(0.1);
        trainer.be_verbose();
        trainer.set_synchronization_file("mmod_sync", std::chrono::minutes(2));
        trainer.set_iterations_without_progress_threshold(3); //300

        //DNN Trainer Set / Cropper
        std::vector<dlib::matrix<dlib::rgb_pixel>> mini_batch_samples;
        std::vector<std::vector<dlib::mmod_rect>>  mini_batch_labels;
        dlib::random_cropper cropper;
        cropper.set_chip_dims(200, 200);
        cropper.set_min_object_size(0.2);
        cropper.set_max_rotation_degrees(90);
        cropper.set_randomly_flip(true);
        dlib::rand rnd;

        //Training durchführen + Cropper + Random Jitter
        std::cout << "Training startet..." << std::endl;
        while(trainer.get_learning_rate() >= 0.0001) {
            std::cout << trainer.get_learning_rate() << std::endl;
            cropper(150, images_train, face_boxes_train, mini_batch_samples, mini_batch_labels); //150
            for (auto&& img : mini_batch_samples) disturb_colors(img, rnd);
            trainer.train_one_step(mini_batch_samples, mini_batch_labels);
        }

        //Training fertig
        trainer.get_net();
        std::cout << "Training fertig." << std::endl;

        //Trainingsstand speichern
        net.clean();
        dlib::serialize("mmod_state.dat") << net;

        //Ergebnisse ausgeben
        //std::cout << "Trainingsergebnis: "  << test_object_detection_function(net, images_train, face_boxes_train) << std::endl;
        //std::cout << "Testergebnis ... :  " << test_object_detection_function(net, images_test,  face_boxes_test)  << std::endl;

        //Einstellungen ausgeben
        std::cout << trainer << cropper << std::endl;

        //Testergebnisse anzeigen
        std::cout << "Pause..." << std::flush;
        std::cin.get();
        dlib::image_window win;
        unsigned int n = 0;
        for (auto&& img : images_test) {
            std::cout << "Lade Bild #" << ++n << std::flush;
            pyramid_up(img);
            auto dets = net(img);
            win.clear_overlay();
            win.set_image(img);
            for (auto&& d : dets) win.add_overlay(d);
            std::cout << "Fertig." << std::endl;
            std::cout << "Weiter?" << std::flush;
            std::cin.get();
        }
    }

private:

    std::string training_xml;
    std::string testing_xml;

    /* dlib Interna */
    template <long num_filters, typename SUBNET> using con5d = dlib::con<num_filters,5,5,2,2,SUBNET>;
    template <long num_filters, typename SUBNET> using con3  = dlib::con<num_filters,3,3,1,1,SUBNET>;
    template <typename SUBNET> using downsampler = dlib::relu<dlib::bn_con<con5d<32, dlib::relu<dlib::bn_con<con5d<32, dlib::relu<dlib::bn_con<con5d<32,SUBNET>>>>>>>>>;
    template <typename SUBNET> using rcon3       = dlib::relu<dlib::bn_con<con3<32,SUBNET>>>;
    using net_type = dlib::loss_mmod<dlib::con<1,6,6,1,1,rcon3<rcon3<rcon3<downsampler<dlib::input_rgb_image_pyramid<dlib::pyramid_down<6>>>>>>>>;

};