//
// Created by krizchri on 21.09.2017.
//
#pragma once

#include "ki.h"
#include <dlib/mlp.h>


/**
 * Maschinenlerntyp:
 * Klassifikation: Implementierung des Multilayer-Perceptrons.
 *
 * @tparam sample_type Datentyp der Werte aus der Datenmatrix. In aller Regel ``double``.
 * @tparam spalten Anzahl der Spalten in der Datenmatrix.
 * @tparam zeilen Anzahl der Zeilen in der Datenmatrix.
 */
template<typename sample_type, unsigned long spalten, unsigned long zeilen>
class KI_C_MLP : private KI {

public:

    /**
     * ctor.
     * @param hidden_layers Anzahl versteckter Schichten, die das MLP implementiert.
     */
    explicit KI_C_MLP(const unsigned long hidden_layers)
            : mlp(spalten, hidden_layers) {

    }

    /**
     * Fügt einen Satz Trainingsdaten hinzu, aus denen sofort gelernt wird.
     * @param sample Zu lernender Datensatz.
     * @param category Wie der zu lernende Datensatz zu kategorisieren ist.
     */
    void add_learn_data(const dlib::matrix<sample_type, spalten, zeilen> sample, const sample_type category) {
        mlp.train(sample, category);
    }

    /**
     * Gibt den Erwartungswert wieder für den als Parameter gereichten Datensatz.
     * MLPs unterliegen zufälliger Initialisierung. Von Durchlauf zu Durchlauf können
     * Vorhersagewerte abweichen.
     * @param sample Datensatz, für den eine Vorhersage getroffen werden soll.
     */
    const sample_type predict(const dlib::matrix<sample_type, spalten, zeilen> sample) const {
        return mlp(sample);
    }


private:

    dlib::mlp::kernel_1a_c mlp;

};