//
// Created by krizchri on 13.09.2017.
//
#pragma once

#include "ki.h"
#include <iostream>
#include <dlib/svm.h>

/**
 * Maschinenlerntyp:
 * Klassifikation: Kernel ridge regression classification
 * - aufteilung nach vordefinierten Klassen
 *
 * @tparam sample_type Datentyp der Werte aus der Datenmatrix. In aller Regel ``double``.
 * @tparam label_type Datentyp der Zielwerte.
 * @tparam spalten Anzahl der Spalten in der Datenmatrix.
 * @tparam zeilen Anzahl der Zeilen in der Datenmatrix.
 */
template<typename sample_type, typename label_type, const long spalten, const long zeilen>
class KI_C_KRR : private KI {

public:

    /**
     * ctor
     */
    explicit KI_C_KRR(const double gamma = 0.000625) {
        trainer.use_classification_loss_for_loo_cv();
        trainer.set_kernel(dlib::radial_basis_kernel<dlib::matrix<sample_type, spalten, zeilen>>(gamma));
    }

    /**
     * Lerndaten hinzufügen.
     */
    void add_learn_data(const dlib::matrix<sample_type, spalten, zeilen> sample, const label_type label) {
        samples.push_back(sample);
        labels.push_back(label);
    }

    /**
     * Trainingsprozess durchführen.
     */
    void train() override {
        std::cout << "KI_C_KRR.train()..." << std::endl;
        //Trainingskorpus *normalisieren*
        normalizer.train(samples);
        for (dlib::matrix<sample_type, spalten, zeilen>& sample : samples) sample = normalizer(sample);
        //Training
        decision_function.normalizer = normalizer;
        decision_function.function = trainer.train(samples, labels);
        std::cout << " fertig.\nAnzahl genutzter Basisvektoren=" << decision_function.function.basis_vectors.size() << std::endl;
    }

    /**
     * Vorhersage treffen.
     */
    const label_type predict(const dlib::matrix<sample_type, spalten, zeilen> sample) const { return decision_function(sample); }

    ///debug
    void debug_gamma() {
        std::cout << "KI_C_KRR::debug_gamma()" << std::endl;
        for (double gamma = 0.000001; gamma <= 1; gamma *= 5) {
            trainer.set_kernel(dlib::radial_basis_kernel<dlib::matrix<sample_type, spalten, zeilen>>(gamma));
            std::vector<sample_type> loo_values;
            trainer.train(samples, labels, loo_values);
            const double classification_accuracy = dlib::mean_sign_agreement(labels, loo_values);
            std::cout << "gamma: " << gamma << "     Sicherheit: " << classification_accuracy << std::endl;
        }
    }



private:
    std::vector
            <dlib::matrix<sample_type, spalten, zeilen>> samples;
    std::vector
            <label_type> labels;
    dlib::vector_normalizer
            <dlib::matrix<sample_type, spalten, zeilen>> normalizer;
    dlib::normalized_function
            <dlib::decision_function
                    <dlib::radial_basis_kernel
                            <dlib::matrix<sample_type, spalten, zeilen>>>> decision_function;
    dlib::krr_trainer
            <dlib::radial_basis_kernel
                    <dlib::matrix<sample_type, spalten, zeilen>>> trainer;

};