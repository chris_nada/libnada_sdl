//
// Created by nada on 25.11.2017.
//
#pragma once

#include <dlib/svm_threaded.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_processing.h>
#include <dlib/data_io.h>

class KI_C_IMAGES {

public:

    KI_C_IMAGES() {}

    void test() {
        // Here we declare an image object that can store color rgb_pixels.
        dlib::array2d<dlib::rgb_pixel> img;
        dlib::array2d<dlib::rgb_pixel> square;
        square.set_size(0xFF, 0xFF);

        // Now load the image file into our image.  If something is wrong then
        // load_image() will throw an exception.  Also, if you linked with libpng
        // and libjpeg then load_image() can load PNG and JPEG files in addition
        // to BMP files.
        std::string image = "data/018.jpg";
        dlib::load_image(img, image);
        //dlib::load_image(square, "data/square.png");


        // Now convert the image into a FHOG feature image.  The output, hog, is a 2D array
        // of 31 dimensional vectors.
        //dlib::resize_image(0.5, square);
        //dlib::resize_image(img, square);
        dlib::array2d<dlib::matrix<float,31,1> > hog;
        dlib::extract_fhog_features(img, hog);

        std::cout << "hog image x/y =  " << hog.nc() << " / " << hog.nc() << std::endl;

        // Let's see what the image and FHOG features look like.
        dlib::image_window win(img);
        dlib::image_window winhog(dlib::draw_fhog(hog));


        // Finally, sometimes you want to get a planar representation of the HOG features
        // rather than the explicit vector (i.e. interlaced) representation used above.
        dlib::array<dlib::array2d<float>> planar_hog;
        extract_fhog_features(img, planar_hog);
        dlib::image_window planar_hog_win(dlib::draw_fhog(planar_hog));
        // Now we have an array of 31 float valued image planes, each representing one of
        // the dimensions of the HOG feature vector.
        win.wait_until_closed();
        winhog.wait_until_closed();
        planar_hog_win.wait_until_closed();
    }


};