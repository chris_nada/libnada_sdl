//
// Created by nada on 31.10.2017.
//
#pragma once

#include <SDL_mixer.h>
#include <SDL_net.h>
#include "gfx/gfx.h"

class Nadalib {

public:

    static bool init(unsigned short width = 1280, unsigned short height = 720, std::string window_title = "OpenGL Window", bool fullscreen = false) {
        std::cout << "Nadalib::init()" << std::endl;

        //INIT SDL
        if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
            std::cerr << "SDL init error: " << SDL_GetError() << std::endl;
            return false;
        }

        //INIT MIXER
        const int samplingFrequency = 44100;    // 44100 Hz Abtastfrequenz
        Uint16 audioFormat = AUDIO_S16SYS;      // 16 Bits pro Sample
        const int numChannels = 2;              // 2 Kanäle = Stereo
        const int chunkSize = 4096;             // ein guter Wert ...
        if (Mix_OpenAudio(samplingFrequency, audioFormat, numChannels, chunkSize) == -1) {
            std::cerr << "SDL init error: " << SDL_GetError() << std::endl;
            return false;
        }

        //INIT SDL_NET
        if (SDLNet_Init() != 0) {
            std::cerr << "SDL_net init error." << SDL_GetError() << std::endl;
            return false;
        }

        //INIT WINDOW
        Gfx::SCREEN_WIDTH  = width;
        Gfx::SCREEN_HEIGHT = height;
        Gfx::WINDOW = SDL_CreateWindow(window_title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                       Gfx::SCREEN_WIDTH, Gfx::SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if (Gfx::WINDOW == NULL) {
            std::cerr << "Window init error. " << SDL_GetError() << std::endl;
            return false;
        }
        if (fullscreen) SDL_SetWindowFullscreen(Gfx::WINDOW, SDL_WINDOW_FULLSCREEN_DESKTOP);
        static SDL_Surface* surface = SDL_GetWindowSurface(Gfx::WINDOW);

        //INIT RENDERER
        Gfx::RENDERER = SDL_CreateRenderer(Gfx::WINDOW, -1, SDL_RENDERER_ACCELERATED);
        if (Gfx::RENDERER == NULL) {
            std::cerr << "Render init error. " << SDL_GetError() << std::endl;
            return false;
        }

        //INIT PNG
        const int imgFlags = IMG_INIT_PNG;
        if (!(IMG_Init(imgFlags) & imgFlags)) {
            std::cerr << "SDL_image init error. " << SDL_GetError() << std::endl;
            return false;
        }

        //INIT SDL_FONT
        if (TTF_Init() != 0) {
            std::cerr << "SDL_image init error. " << SDL_GetError() << std::endl;
            return false;
        }

        SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2");

        Gfx::FONT_TEXT  = TTF_OpenFont("data/dejavuserif.ttf", 14);
        Gfx::FONT_TINY  = TTF_OpenFont("data/dejavuserif.ttf", 10);
        Gfx::FONT_LABEL = TTF_OpenFont("data/dejavuserif.ttf", 16);
        Gfx::FONT_SANS  = TTF_OpenFont("data/dejavusans.ttf",  16);
        Gfx::FONT_MONO  = TTF_OpenFont("data/dejavumono.ttf",  16);
        if (Gfx::FONT_LABEL == NULL || Gfx::FONT_SANS == NULL || Gfx::FONT_MONO == NULL || Gfx::FONT_TEXT == NULL) {
            std::cerr << "SDL_font init error. " << SDL_GetError() << std::endl;
            return false;
        }
        return true;
    }

    static void close() {
        std::cout << "Nadalib::close() { " << std::flush;
        Gfx::finalize();
        SDL_DestroyRenderer(Gfx::RENDERER); Gfx::RENDERER = NULL;
        SDL_DestroyWindow(Gfx::WINDOW);     Gfx::WINDOW   = NULL;
        SDLNet_Quit();
        TTF_Quit();
        IMG_Quit();
        SDL_Quit();
        std::cout << " } done." << std::endl;
    }

};