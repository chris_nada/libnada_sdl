#pragma once

#include <algorithm>
#include "../gfx/gfx.h"
#include "node.h"

class Textinput : public Node {

public:

    //ctor Default
    Textinput(int x, int y, std::string& text);

    //ctor +
    Textinput(int x, int y, uint16_t width, uint16_t height, std::string& text, uint8_t size);

    void update(const SDL_Event* event);

    void render() const override;

    const std::string& get_input() const { return input; }


protected:

    std::string text;
    std::string input;
    uint8_t     size;

};