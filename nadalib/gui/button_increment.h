#pragma once

#include "Button.h"

class IncrementButton : public Button {

public:
    ///ctor
    IncrementButton(int x, int y, std::string text, int value, int increment, int min, int max);

    void render() const override;

    void update(const SDL_Event* event);

    int get_value() const { return value; }
    int get_increment() const { return increment; }

    void set_value(int value) { this->value = value; }
    void set_increment(int increment) { this->increment = increment; }

private:
    int value;
    int increment;
    int min, max;


};