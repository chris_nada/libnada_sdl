//
// Created by nada on 13.10.2017.
//

#include <SDL_ttf.h>
#include "label.h"
#include "../gfx/gfx.h"

Label::Label(int x, int y, const std::string& text, SDL_Color font_color)
        : text(text), font_color(font_color) {
    SDL_Surface* text_surface = TTF_RenderUTF8_Blended(Gfx::FONT_LABEL, Label::text.c_str(), Label::font_color);
    text_texture = SDL_CreateTextureFromSurface(Gfx::RENDERER, text_surface);
    r1.x = 0;
    r1.y = 0;
    r1.w = text_surface->w;
    r1.h = text_surface->h;
    r2.x = x;
    r2.y = y;
    r2.w = text_surface->w;
    r2.h = text_surface->h;
    SDL_FreeSurface(text_surface);
    render();
}

void Label::set_text(const std::string& text) {
    if (Label::text != text) {
        Label::text = text;
        SDL_RenderFillRect(Gfx::RENDERER, &r2);
        SDL_Surface* text_surface = TTF_RenderUTF8_Blended(Gfx::FONT_LABEL, Label::text.c_str(), font_color);
        text_texture = SDL_CreateTextureFromSurface(Gfx::RENDERER, text_surface);
        r1.w = text_surface->w;
        r1.h = text_surface->h;
        r2.w = text_surface->w;
        r2.h = text_surface->h;
        SDL_FreeSurface(text_surface);
        render();
    }
}

Label::~Label() {
    SDL_DestroyTexture(text_texture);
}

void Label::render() const {
    SDL_RenderCopy(Gfx::RENDERER, text_texture, &r1, &r2);
}
