//
// Created by nada on 12.10.2017.
//
#include "button_increment.h"

IncrementButton::IncrementButton(int x, int y, std::string text, int value, int increment, int min, int max)
        :Button(x, y, 60, 40, text),
         value(value),
         increment(increment),
         min(min), max(max) {
    this->render();
}

void IncrementButton::render() const {
    if (mouse_over()) SDL_SetRenderDrawColor( Gfx::RENDERER, Gfx::RGB_BG2[0], Gfx::RGB_BG2[1], Gfx::RGB_BG2[2], 0xFF);
    else SDL_SetRenderDrawColor( Gfx::RENDERER, Gfx::RGB_BG[0], Gfx::RGB_BG[1], Gfx::RGB_BG[2], 0xFF);
    SDL_RenderFillRect( Gfx::RENDERER, &rect );
    SDL_SetRenderDrawColor( Gfx::RENDERER, 0x00, 0x00, 0x00, 0xFF );
    Gfx::blit_text(text + std::to_string(value), rect.x + 12, rect.y + rect.h / 2 - 8);
}

void IncrementButton::update(const SDL_Event* event) {
    if (event->wheel.y == 1 || event->wheel.y == -1) {
        int mx, my;
        SDL_GetMouseState(&mx, &my);
        if (mx < rect.x) return;
        if (mx > rect.x + rect.w) return;
        if (my < rect.y) return;
        if (my > rect.y + rect.h) return;
        value += increment * event->wheel.y;
        if (value < min) {
            value = min;
            return;
        }
        if (value > max) {
            value = max;
            return;
        }
        render();
    }
}
