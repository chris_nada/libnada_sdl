//
// Created by nada on 12.10.2017.
//

#include "textarea.h"

Textarea::Textarea(int x, int y, const std::string& text, unsigned int width, SDL_Color font_color)
        : width(width) {
    this->text = text;
    SDL_Surface* text_surface = TTF_RenderUTF8_Blended_Wrapped(Gfx::FONT_TEXT, this->text.c_str(), font_color, width);
    text_texture = SDL_CreateTextureFromSurface(Gfx::RENDERER, text_surface);
    r1.x = 0;
    r1.y = 0;
    r1.w = text_surface->w;
    r1.h = text_surface->h;
    r2.x = x;
    r2.y = y;
    r2.w = text_surface->w;
    r2.h = text_surface->h;
    rect.x = x - 4;
    rect.y = y - 4;
    rect.w = r2.w + 8;
    rect.h = r2.h + 8;
    SDL_FreeSurface(text_surface);
    render();
}

void Textarea::render() const {
    SDL_SetRenderDrawColor(Gfx::RENDERER, Gfx::RGB_BG[0], Gfx::RGB_BG[1], Gfx::RGB_BG[2], 0xFF);
    SDL_RenderFillRect(Gfx::RENDERER, &rect);
    SDL_RenderCopy(Gfx::RENDERER, text_texture, &r1, &r2);
    SDL_SetRenderDrawColor(Gfx::RENDERER, 0x00, 0x00, 0x00, 0xFF);
}
