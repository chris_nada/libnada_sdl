//
// Created by nada on 02.10.2017.
//
#pragma once

#include <SDL_rect.h>

class Node {

public:

    Node() : active(true) {};
    virtual ~Node() = default;

    virtual void render() const = 0;

    void set_position(int x, int y) {
        rect.x = x;
        rect.y = y;
    }

    const SDL_Rect& get_rect() const { return rect; }
    bool is_active() const { return active; }
    void set_active(bool active) { this->active = active; }

protected:

    SDL_Rect rect;
    bool active;

};