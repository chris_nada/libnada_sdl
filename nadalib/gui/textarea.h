//
// Created by nada on 12.10.2017.
//
#pragma once

#include "../common/commons.h"
#include "../gfx/gfx.h"
#include "label.h"

class Textarea : public Label {

public:

    Textarea(int x, int y, const std::string& text,
             unsigned int width = 160,
             SDL_Color font_color = {0xFF, 0xFF, 0xFF, 0xFF});

    void render() const override;

private:

    unsigned int width;

};
