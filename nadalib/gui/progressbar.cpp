//
// Created by nada on 12.10.2017.
//
#include "progressbar.h"

Progressbar::Progressbar(int x, int y, uint8_t value, uint8_t red, uint8_t green, uint8_t blue)
        : red(red), green(green), blue(blue) {
    rect.x = x;
    rect.y = y;
    rect.w = 208;
    rect.h = 20;
    rect_progress.x = x + 4;
    rect_progress.y = y + 4;
    rect_progress.w = value * 2;
    rect_progress.h = rect.h - 8;
    render();
}

void Progressbar::render() const {
    SDL_SetRenderDrawColor(Gfx::RENDERER, Gfx::RGB_BG[0], Gfx::RGB_BG[1], Gfx::RGB_BG[2], 0xFF);
    SDL_RenderFillRect(Gfx::RENDERER, &rect);
    SDL_SetRenderDrawColor(Gfx::RENDERER, 200-rect_progress.w, rect_progress.w, blue, 0xFF);
    SDL_RenderFillRect(Gfx::RENDERER, &rect_progress);
    SDL_SetRenderDrawColor(Gfx::RENDERER, 0x00, 0x00, 0x00, 0xFF);
}

void Progressbar::set_value(uint8_t value) {
    rect_progress.w = value * 2;
    render();
}

