//
// Created by nada on 13.10.2017.
//
#pragma once

#include <SDL_render.h>
#include <string>
#include "node.h"

class Label : public Node {

public:

    Label() = default;

    Label(int x, int y, const std::string& text, SDL_Color font_color = {0xFF, 0xFF, 0xFF, 0xFF});

    virtual ~Label();

    void render() const override;

    virtual void set_text(const std::string& text);

protected:

    std::string text;
    SDL_Rect r1, r2;
    SDL_Texture* text_texture;
    SDL_Color font_color;

};