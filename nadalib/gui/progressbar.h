//
// Created by nada on 02.10.2017.
//
#pragma once

#include "node.h"
#include "../gfx/gfx.h"

class Progressbar : public Node {

public:

    Progressbar(int x, int y,
                uint8_t value,
                uint8_t red = 0x80, uint8_t green = 0x80, uint8_t blue = 0x80);

    void render() const override;

    void set_value(uint8_t value);


protected:

    SDL_Rect rect_progress;
    uint8_t red;
    uint8_t green;
    uint8_t blue;

};