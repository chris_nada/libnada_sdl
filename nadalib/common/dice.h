#pragma once

#include <vector>
#include <random>
#include <ctime>
#include <iostream>
#include <chrono>
#include "dice.h"

class Dice {

public:

    static int range(int min, int max);

    /**
     * Liefert eine maschinengenerierte psudo-Zufallszahl (double mit 6 Nachkommastellen)
     * im angegebenen Zahlenbereich. Grenzen sind jeweils einschließlich.
     * @param min untere einschließliche Grenze der Zufallszahl
     * @param max obere einschließliche Grenze der Zufallszahl
     * @return die generierte Zufallszahl
     */
    static double random_double(double min, double max);

    /**
     * Liefert eine maschinengenerierte psudo-Zufallszahl (int) im angegebenen Zahlenbereich.
     * Grenzen sind jeweils einschließlich.
     * @param min untere einschließliche Grenze der Zufallszahl
     * @param max obere einschließliche Grenze der Zufallszahl
     * @return die generierte Zufallszahl
     */
    static int range_b(int min, int max);

    static bool chance(int percent);


private:

    Dice() = default;
    static long long seed;
    static long long init() {
        std::cout << "Dice::init()" << std::endl;
        std::srand((unsigned int) std::clock());
        return std::chrono::system_clock::now().time_since_epoch().count();;
    }

};
