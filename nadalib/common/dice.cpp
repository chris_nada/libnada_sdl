//
// Created by nada on 10.10.2017.
//

#include "dice.h"

long long Dice::seed = init();

double Dice::random_double(double min, double max) {
    if (min >= max) return min;
    return (min + ((double)rand() / RAND_MAX) * (max - min));
}

int Dice::range(int min, int max) {
    if (min >= max) return min;
    return ((rand() % (max - min)) + min);
}

int Dice::range_b(int min, int max) {
    if (min >= max) return min;
    if (min == 0) return range_b(min + 1, max + 1) - 1;
    if (min < 0 || max < 0) return 0;
    std::mt19937_64 dre(seed);
    std::uniform_int_distribution<int> uid(min, max);
    int n = uid(dre);
    seed += n;
    return n;
}

bool Dice::chance(int percent) {
    if (percent <= 0) return false;
    return range_b(1, 100) <= percent;
}


