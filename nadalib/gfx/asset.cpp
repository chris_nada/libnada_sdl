//
// Created by nada on 03.10.2017.
//
#include "asset.h"
#include "gfx.h"

Asset::Asset(std::string& path) {
    SDL_Surface* loaded_surface = IMG_Load(path.c_str());
    if (loaded_surface == NULL) {
        std::cerr << "Gfx::get_texture_from_path(" << path << ") could not be loaded: " << IMG_GetError() << std::endl;
    }
    texture = SDL_CreateTextureFromSurface(Gfx::RENDERER, loaded_surface);
    if (texture == NULL) {
        std::cerr << "Gfx::get_texture_from_path(" << path << ") could not be loaded: " << SDL_GetError() << std::endl;
    }
    SDL_FreeSurface(loaded_surface);
    //float multi = 1.0f; //resize
    r1.x = 0;
    r1.y = 0;
    r1.w = loaded_surface->w;
    r1.h = loaded_surface->h;
    r2.x = 0;
    r2.y = 0;
    r2.w = loaded_surface->w; //* multi;
    r2.h = loaded_surface->h; //* multi;
}

void Asset::render() {
    Gfx::blit(texture, &r1, &r2);
}

void Asset::render(int x, int y) {
    r2.x = x;
    r2.y = y;
    Gfx::blit(texture, &r1, &r2);
}

Asset::~Asset() {
    if (texture) SDL_DestroyTexture(texture);
}
