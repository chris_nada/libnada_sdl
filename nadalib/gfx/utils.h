#pragma once

#include <vector>

class Utils {

public:

    static void draw_clock(int x, int y, int time_h, int time_min, bool time_am, bool string = false);
    static void draw_dot_cloud(const std::vector<std::tuple<double, double, SDL_Color>>& values, bool connect = false);

};