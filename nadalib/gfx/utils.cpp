#include <SDL2_gfxPrimitives.h>
#include "utils.h"
#include "gfx.h"

void Utils::draw_clock(const int x, const int y, const int time_h, const int time_min, const bool time_am, const bool string) {
    //blit_rect(x-32, y, 96, 128);
    filledCircleRGBA(Gfx::RENDERER, x, y+48, 56, 0x20, 0x20, 0x20, 0xFF);
    filledCircleRGBA(Gfx::RENDERER, x, y+48, 48, 0x40, 0x40, 0x40, 0xFF);
    SDL_Point* points = new SDL_Point[4]{{x, y+4}, {x, y+92}, {x-44, y+48}, {x+44, y+48}};
    SDL_SetRenderDrawColor(Gfx::RENDERER, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderDrawPoints(Gfx::RENDERER, points, 4);
    SDL_SetRenderDrawColor(Gfx::RENDERER, 0x00, 0x00, 0x00, 0xFF);
    delete points;
    double deg = time_h*30 + time_min/2;
    double deg2 = time_min * 6;
    lineRGBA(Gfx::RENDERER, x, 64,
             x + (sin((540 - deg) * (M_PI / 180)) * 24),
             64 + (cos((540 - deg) * (M_PI / 180)) * 24), 0xFF, 0xFF, 0xFF, 0xFF);
    lineRGBA(Gfx::RENDERER, x, 64,
             x + (sin((540 - deg2) * (M_PI / 180)) * 40),
             64 + (cos((540 - deg2) * (M_PI / 180)) * 40), 0xFF, 0xFF, 0xFF, 0xFF);
    if (string) {
        std::string time;
        if (time_min < 10) {
            time = std::to_string(time_h) + ":0" + std::to_string(time_min);
        } else {
            time = std::to_string(time_h) + ":" + std::to_string(time_min);
        }
        if (time_am) time += " AM";
        else         time += " PM";
        Gfx::blit_text(time, x - 32, y + 100);
    }
}


void Utils::draw_dot_cloud(const std::vector<std::tuple<double, double, SDL_Color>>& values, const bool connect) {
    //Request Focus
    SDL_RaiseWindow(Gfx::WINDOW);

    //Config
    double width  = Gfx::SCREEN_WIDTH;
    double height = Gfx::SCREEN_HEIGHT;
    unsigned short padding = 8;

    //Clear
    SDL_SetRenderDrawColor(Gfx::RENDERER, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderClear(Gfx::RENDERER);
    SDL_SetRenderDrawColor(Gfx::RENDERER, 0x00, 0x00, 0x00, 0xFF);
    if (values.empty()) {
        std::cerr << "Utils::draw_dot_cloud(), values empty." << std::endl;
        return;
    }

    //Min und Max Werte finden zum Skalieren
    double min_x = std::get<0>(values[0]);
    double min_y = std::get<1>(values[0]);
    double max_x = std::get<0>(values[0]);
    double max_y = std::get<1>(values[0]);
    for (std::tuple<double, double, SDL_Color> tuple : values) {
        if (std::get<0>(tuple) < min_x && std::isfinite(std::get<0>(tuple))) min_x = std::get<0>(tuple);
        if (std::get<0>(tuple) > max_x && std::isfinite(std::get<0>(tuple))) max_x = std::get<0>(tuple);
        if (std::get<1>(tuple) < min_y && std::isfinite(std::get<1>(tuple))) min_y = std::get<1>(tuple);
        if (std::get<1>(tuple) > max_y && std::isfinite(std::get<1>(tuple))) max_y = std::get<1>(tuple);
    }
    std::cout << "Rand. = " << min_x << ", " << max_x << ", " << min_y << ", " << max_y << std::endl;

    //Skalierung
    double scale_x = abs((0.5*width - padding) / (abs(min_x) > abs(max_x) ? min_x : max_x));
    double scale_y = abs((0.5*width - padding) / (abs(min_y) > abs(max_y) ? min_y : max_y));
    std::cout << "Skal. = " << scale_x << ", " << scale_y << std::endl;

    //Koordinatensystem zeichnen
    SDL_Point root = {(int)(0.5 * width), (int)(0.5 * height)};
    SDL_RenderDrawLine(Gfx::RENDERER, padding, root.y, (int)(width - padding), root.y);  //X-Achse
    SDL_RenderDrawLine(Gfx::RENDERER, root.x, padding, root.x, (int)(height - padding)); //Y-Achse

    //Korrdinatensystem beschriften - erst negativer Bereich, dann positiver
    for (double k = -3.0; k <= 3.1; ++k) {
        if ((int)k == 0) continue;
        double x = ((abs(min_x) > abs(max_x) ? min_x : max_x) / 3.0) * k;
        double y = ((abs(min_y) > abs(max_y) ? min_y : max_y) / 3.0) * k;
        double screen_x = root.x + x * scale_x;
        double screen_y = root.y + y * scale_y;
        SDL_RenderDrawLine(Gfx::RENDERER, (int)screen_x, root.y - 2, (int)screen_x, root.y + 2);
        SDL_RenderDrawLine(Gfx::RENDERER, root.x - 2, (int)screen_y, root.x + 2, (int)screen_y);
        Gfx::blit_text(std::to_string((int)x),  (int)screen_x - 4, root.y + 8, Gfx::FONT_TINY, {0x00, 0x00, 0x00, 0xFF});
        Gfx::blit_text(std::to_string((int)-y), root.x + 8, (int)screen_y - 4, Gfx::FONT_TINY, {0x00, 0x00, 0x00, 0xFF});
    }

    //Punkte zeichnen
    for (std::tuple<double, double, SDL_Color> tuple : values) {
        if (std::isfinite(std::get<0>(tuple)) && std::isfinite(std::get<1>(tuple))) {
            SDL_SetRenderDrawColor(Gfx::RENDERER,
                                   std::get<SDL_Color>(tuple).r,
                                   std::get<SDL_Color>(tuple).g,
                                   std::get<SDL_Color>(tuple).b,
                                   std::get<SDL_Color>(tuple).a);
            double x = root.x + std::get<0>(tuple) * scale_x;
            double y = root.y - std::get<1>(tuple) * scale_y;
            SDL_RenderDrawPoint(Gfx::RENDERER, x,     y);
            SDL_RenderDrawPoint(Gfx::RENDERER, x + 1, y);
            SDL_RenderDrawPoint(Gfx::RENDERER, x,     y + 1);
            SDL_RenderDrawPoint(Gfx::RENDERER, x + 1, y + 1);
        }
    }

    //Rendern
    SDL_RenderPresent(Gfx::RENDERER);
    SDL_SetRenderDrawColor(Gfx::RENDERER, 0x00, 0x00, 0x00, 0xFF);
}
