#include "gfx.h"
#include <string>
#include <iostream>
#include <SDL_render.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL2_gfxPrimitives.h>


unsigned short       Gfx::SCREEN_WIDTH  = 1280;
unsigned short       Gfx::SCREEN_HEIGHT =  720;
uint8_t              Gfx::RGB_BG[]  = {0x29, 0x29, 0x37};
uint8_t              Gfx::RGB_BG2[] = {0x60, 0x60, 0x70};
SDL_Window*          Gfx::WINDOW;
SDL_Renderer*        Gfx::RENDERER;
TTF_Font*            Gfx::FONT_LABEL;
TTF_Font*            Gfx::FONT_SANS;
TTF_Font*            Gfx::FONT_MONO;
TTF_Font*            Gfx::FONT_TEXT;
TTF_Font*            Gfx::FONT_TINY;
std::map<std::string, std::unique_ptr<Asset>> Gfx::assets;


void Gfx::blit(SDL_Texture* texture, const SDL_Rect* r1, const SDL_Rect* r2) {
    SDL_RenderCopy(RENDERER, texture, r1, r2);
}


void Gfx::blit(const std::string& path, const int x, const int y) {
    SDL_Surface* loadedSurface = IMG_Load((path).c_str());
    if (loadedSurface == NULL) {
        std::cerr << "Gfx::blit(" << path << ") could not be loaded: " << SDL_GetError() << std::endl;
        return;
    }
    SDL_Texture* newTexture = SDL_CreateTextureFromSurface( Gfx::RENDERER, loadedSurface );
    if (newTexture == NULL) {
        std::cerr << "Gfx::blit(" << path << ") could not be loaded: " << SDL_GetError() << std::endl;
        SDL_FreeSurface(loadedSurface);
        return;
    }
    SDL_Rect r1;
    SDL_Rect r2;
    r1.w = loadedSurface->w;
    r1.h = loadedSurface->h;
    r1.x = 0;
    r1.y = 0;
    r2.w = loadedSurface->w;
    r2.h = loadedSurface->h;
    r2.x = x;
    r2.y = y;
    SDL_RenderCopy(Gfx::RENDERER, newTexture, &r1, &r2);
    SDL_FreeSurface(loadedSurface);
    SDL_DestroyTexture(newTexture);
}


void Gfx::blit_fullscreen(const std::string& path) {
    SDL_Surface* loadedSurface = IMG_Load((path).c_str());
    if (loadedSurface == NULL) {
        std::cerr << "Gfx::blit(" << path << ") could not be loaded: " << SDL_GetError() << std::endl;
        return;
    }
    SDL_Texture* newTexture = SDL_CreateTextureFromSurface( Gfx::RENDERER, loadedSurface );
    if (newTexture == NULL) {
        std::cerr << "Gfx::blit(" << path << ") could not be loaded: " << SDL_GetError() << std::endl;
        SDL_FreeSurface(loadedSurface);
        return;
    }
    SDL_Rect r1;
    SDL_Rect r2;
    float multi = ((float)SCREEN_HEIGHT / (float)(loadedSurface->h));
    r1.w = loadedSurface->w;
    r1.h = loadedSurface->h;
    r1.x = 0;
    r1.y = 0;
    r2.w = loadedSurface->w * multi;
    r2.h = loadedSurface->h * multi;
    r2.x = (SCREEN_WIDTH - r2.w)/2;
    r2.y = 0;
    SDL_RenderCopy(Gfx::RENDERER, newTexture, &r1, &r2);

    SDL_FreeSurface(loadedSurface);
    SDL_DestroyTexture(newTexture);
}


void Gfx::blit_sized(const std::string& path, const int x, const int y, const unsigned int max_size, bool center) {
    SDL_Surface* loaded_surface = IMG_Load((path).c_str());
    if (loaded_surface == NULL) {
        std::cerr << "Gfx::blit(" << path << ") could not be loaded: " << SDL_GetError() << std::endl;
        return;
    }
    SDL_Texture* newTexture = SDL_CreateTextureFromSurface( Gfx::RENDERER, loaded_surface );
    if (newTexture == NULL) {
        std::cerr << "Gfx::blit(" << path << ") could not be loaded: " << SDL_GetError() << std::endl;
        SDL_FreeSurface(loaded_surface);
        return;
    }

    SDL_Rect r1;
    SDL_Rect r2;
    r1.w = loaded_surface->w;
    r1.h = loaded_surface->h;
    r1.x = 0;
    r1.y = 0;
    r2.x = x;
    r2.y = y;
    float multi = (float) max_size;
    if (r1.w > r1.h) {
        r2.w = (int) multi;
        r2.h = (int)(multi * ((float)r1.h) / (float)r1.w);
        if (center) r2.y += 0.5 * (max_size - r2.h);
    }
    else {
        r2.h = (int) multi;
        r2.w = (int)(multi * ((float)r1.w / (float)r1.h));
        if (center) r2.x += 0.5 * (max_size - r2.w);
    }

    SDL_RenderCopy(Gfx::RENDERER, newTexture, &r1, &r2);
    SDL_FreeSurface(loaded_surface);
    SDL_DestroyTexture(newTexture);
}


void Gfx::blit_sized(const std::string& path, const int x, const int y, const unsigned int max_size, const uint8_t transparency) {
    SDL_Surface* loaded_surface = IMG_Load((path).c_str());
    if (loaded_surface == NULL) {
        std::cerr << "Gfx::blit(" << path << ") could not be loaded: " << SDL_GetError() << std::endl;
        return;
    }
    SDL_Texture* newTexture = SDL_CreateTextureFromSurface( Gfx::RENDERER, loaded_surface );
    if (newTexture == NULL) {
        std::cerr << "Gfx::blit(" << path << ") could not be loaded: " << SDL_GetError() << std::endl;
        SDL_FreeSurface(loaded_surface);
        return;
    }
    SDL_SetTextureAlphaMod(newTexture, transparency);

    SDL_Rect r1;
    SDL_Rect r2;
    r1.w = loaded_surface->w;
    r1.h = loaded_surface->h;
    r1.x = 0;
    r1.y = 0;
    r2.x = x;
    r2.y = y;
    float multi = (float) max_size;
    if (r1.w > r1.h) {
        r2.w = (int) multi;
        r2.h = (int)(multi * ((float)r1.h) / (float)r1.w);
    }
    else {
        r2.h = (int) multi;
        r2.w = (int)(multi * ((float)r1.w / (float)r1.h));
    }

    SDL_RenderCopy(Gfx::RENDERER, newTexture, &r1, &r2);
    SDL_FreeSurface(loaded_surface);
    SDL_DestroyTexture(newTexture);
}


void Gfx::blit_text(const std::string& content, const int x, const int y, TTF_Font* font, const SDL_Color font_color) {
    SDL_Surface* text = TTF_RenderUTF8_Solid(font, content.c_str(), font_color);
    if (text == NULL) {
        std::cerr << "Gfx::blit_text(...) could not be loaded: " << SDL_GetError() << std::endl;
        return;
    }
    SDL_Texture* textt = SDL_CreateTextureFromSurface(Gfx::RENDERER, text);
    if (textt == NULL) {
        std::cerr << "Gfx::blit_text(...) could not be loaded: " << SDL_GetError() << std::endl;
        SDL_FreeSurface(text);
        return;
    }
    SDL_Rect r1;
    SDL_Rect r2;
    r1.w = text->w;
    r1.h = text->h;
    r1.x = 0;
    r1.y = 0;
    r2.w = text->w;
    r2.h = text->h;
    r2.x = x;
    r2.y = y;
    SDL_RenderCopy(Gfx::RENDERER, textt, &r1, &r2);
    SDL_FreeSurface(text);
    SDL_DestroyTexture(textt);
}


void Gfx::blit_rect(const int x, const int y, const int w, const int h, const bool black) {
    SDL_Rect r;
    r.x = x;
    r.y = y;
    r.w = w;
    r.h = h;
    if (!black) SDL_SetRenderDrawColor(RENDERER, 0xFF, 0x00, 0x00, 0x00);
    SDL_RenderFillRect(RENDERER, &r);
    SDL_SetRenderDrawColor(RENDERER, 0x00, 0x00, 0x00, 0xFF);
}


void Gfx::blit_rect(const int x, const int y, const int w, const int h, uint8_t r, uint8_t g, uint8_t b) {
    SDL_Rect rect;
    rect.x = x;
    rect.y = y;
    rect.w = w;
    rect.h = h;
    SDL_SetRenderDrawColor(RENDERER, r, g, b, 0xFF);
    SDL_RenderFillRect(RENDERER, &rect);
    SDL_SetRenderDrawColor(RENDERER, 0x00, 0x00, 0x00, 0xFF);
}


void Gfx::blit_asset(const std::string& path, const int x, const int y) {
    if (assets.count(path) == 0) {
        std::string path_copy = path;
        assets[path] = std::make_unique<Asset>(path_copy);
    }
    assets[path]->render(x, y);
}

void Gfx::finalize() {
    std::cout << "Gfx::finalize()..." << std::flush;
    //TODO
}
